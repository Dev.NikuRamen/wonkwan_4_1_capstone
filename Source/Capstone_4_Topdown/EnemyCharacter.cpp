// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = 100;
	MaxHealth = 100;
	IsDead = false;
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	

}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::OnHit_Implementation(const int Damage)
{
	Health -= Damage;
	if(Health <= 0)
	{
		OnDie();
	}
}

void AEnemyCharacter::OnHeal_Implementation(const int Amount)
{
	Health += Amount;
	if(Health >= MaxHealth)
	{
		Health = MaxHealth;
	}
}

void AEnemyCharacter::OnDie_Implementation()
{
	IsDead = true;
	Health = 0;
}

void AEnemyCharacter::OnAttack_Implementation()
{
	
}
