// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Capstone_4_Topdown : ModuleRules
{
	public Capstone_4_Topdown(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Json" });
	}
}
