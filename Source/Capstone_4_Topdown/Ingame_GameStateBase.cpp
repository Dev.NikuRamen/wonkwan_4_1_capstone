// Fill out your copyright notice in the Description page of Project Settings.


#include "Ingame_GameStateBase.h"
#include "EnemyCharacter.h"
#include "TeamCharacter.h"
#include "Kismet/GameplayStatics.h"

void AIngame_GameStateBase::BeginPlay()
{
	Super::BeginPlay();

	LoadIni();
	UpdateActorCount();
}

void AIngame_GameStateBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	OnUpdateCount.Clear();
}

void AIngame_GameStateBase::UpdateActorCount()
{
	UWorld* World = GetWorld();
	if(!World)
	{
#if UE_EDITOR
		UE_LOG(LogTemp,Warning,TEXT("No world detected!"));
#endif
	}
	TArray<AActor*> enemys;
	TArray<AActor*> teams;

	UGameplayStatics::GetAllActorsOfClass(World,ClassOfEnemy,enemys);
	UGameplayStatics::GetAllActorsOfClass(World,ClassOfTeam,teams);
		
	EnemyCount = enemys.Num();
	TotalEnemyCount = enemys.Num();
	TotalTeamCount = teams.Num();

	int rescued = 0;
	for (auto teamActor : teams)
	{
		auto team = Cast<ATeamCharacter>(teamActor);
		if(team && team->GetIsRescued())
		{
			rescued++;
		}
	}
	RescuedTeamCount = rescued;

	if(OnUpdateCount.IsBound() == true)
	{
		OnUpdateCount.Broadcast();
	}
}

void AIngame_GameStateBase::ChangeCharacterCount(EFieldCharacter Character, int Amount)
{
	switch (Character)
	{
		case Enemy:
			EnemyCount += Amount;
			break;
		case TotalTeam:
			TotalTeamCount += Amount;
			break;
		case RescuedTeam:
			RescuedTeamCount += Amount;
			break;
		case MAX: break;
		default: ;
	}

	if(OnUpdateCount.IsBound() == true)
	{
		OnUpdateCount.Broadcast();
	}
}

void AIngame_GameStateBase::LoadIni()
{
	if(GConfig)
	{
		GConfig->GetInt(TEXT("Sound"),TEXT("MasterVolume"),MasterVolume,GGameIni);
		GConfig->GetInt(TEXT("Sound"),TEXT("SFXVolume"),SFXVolume,GGameIni);
		GConfig->GetInt(TEXT("Sound"),TEXT("VoiceVolume"),VoiceVolume,GGameIni);
		GConfig->GetInt(TEXT("Sound"),TEXT("BGMVolume"),BGMVolume,GGameIni);
#if UE_EDITOR
		UE_LOG(LogTemp,Warning,TEXT("MasterVolume : %d"),MasterVolume);
		UE_LOG(LogTemp,Warning,TEXT("SFXVolume : %d"),SFXVolume);
		UE_LOG(LogTemp,Warning,TEXT("VoiceVolume : %d"),VoiceVolume);
		UE_LOG(LogTemp,Warning,TEXT("BGMVolume : %d"),BGMVolume);
#endif
	}
}

void AIngame_GameStateBase::SaveIni()
{
	if(GConfig)
	{
		GConfig->SetInt(TEXT("Sound"),TEXT("MasterVolume"),MasterVolume,GGameIni);
		GConfig->SetInt(TEXT("Sound"),TEXT("SFXVolume"),SFXVolume,GGameIni);
		GConfig->SetInt(TEXT("Sound"),TEXT("VoiceVolume"),VoiceVolume,GGameIni);
		GConfig->SetInt(TEXT("Sound"),TEXT("BGMVolume"),BGMVolume,GGameIni);
#if UE_EDITOR
		UE_LOG(LogTemp,Warning,TEXT("MasterVolume : %d"),MasterVolume);
		UE_LOG(LogTemp,Warning,TEXT("SFXVolume : %d"),SFXVolume);
		UE_LOG(LogTemp,Warning,TEXT("VoiceVolume : %d"),VoiceVolume);
		UE_LOG(LogTemp,Warning,TEXT("BGMVolume : %d"),BGMVolume);
#endif
	}
}

float AIngame_GameStateBase::GetMasterVolume() const
{
	return MasterVolume;
}

float AIngame_GameStateBase::GetSFXVolume() const
{
	return SFXVolume;
}

float AIngame_GameStateBase::GetVoiceVolume() const
{
	return VoiceVolume;
}

float AIngame_GameStateBase::GetBGMVolume() const
{
	return BGMVolume;
}

void AIngame_GameStateBase::SetMasterVolume(float Value)
{
	MasterVolume = Value;
#if UE_EDITOR
	UE_LOG(LogTemp,Warning,TEXT("Master : %d"),MasterVolume);
#endif
}

void AIngame_GameStateBase::SetSFXVolume(float Value)
{
	SFXVolume = Value;
#if UE_EDITOR
	UE_LOG(LogTemp,Warning,TEXT("SFX : %d"),SFXVolume);
#endif
}

void AIngame_GameStateBase::SetVoiceVolume(float Value)
{
	VoiceVolume = Value;
#if UE_EDITOR
	UE_LOG(LogTemp,Warning,TEXT("Voice : %d"),VoiceVolume);
#endif
}

void AIngame_GameStateBase::SetBGMVolume(float Value)
{
	BGMVolume = Value;
#if UE_EDITOR
	UE_LOG(LogTemp,Warning,TEXT("BGM : %d"),BGMVolume);
#endif
}

void AIngame_GameStateBase::SetGameState(int State)
{
	GameState = State;
}

int AIngame_GameStateBase::GetGameState() const
{
	return GameState;
}
