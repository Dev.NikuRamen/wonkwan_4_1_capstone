// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Ingame_GameStateBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUpdateCount);

UENUM()
enum EFieldCharacter
{
	Enemy,
	TotalTeam,
	RescuedTeam,

	
	MAX
};
/**
 * 
 */
UCLASS(Config=Game,DefaultConfig)
class CAPSTONE_4_TOPDOWN_API AIngame_GameStateBase : public AGameStateBase
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
protected:
	/** Property of in-game enemy count. */
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Enemy)
	TSubclassOf<class AEnemyCharacter> ClassOfEnemy; 
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Enemy)
	int EnemyCount;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Enemy)
	int TotalEnemyCount;

	/** Property of in-game team count. */
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Team)
	TSubclassOf<class ATeamCharacter> ClassOfTeam;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Team)
	int TotalTeamCount;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Team)
	int RescuedTeamCount;

	UPROPERTY(Config,EditAnywhere,BlueprintReadWrite,Category=Option)
	int MasterVolume;
	UPROPERTY(Config,EditAnywhere,BlueprintReadWrite,Category=Option)
	int SFXVolume;
	UPROPERTY(Config,EditAnywhere,BlueprintReadWrite,Category=Option)
	int VoiceVolume;
	UPROPERTY(Config,EditAnywhere,BlueprintReadWrite,Category=Option)
	int BGMVolume;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=GameState)
	int GameState;
	

public:
	/** @brief Calculate all teams and team count of current world. */
	UFUNCTION(BlueprintCallable,Category=Update)
	void UpdateActorCount();
	UFUNCTION(BlueprintCallable,Category=Update)
	void ChangeCharacterCount(EFieldCharacter Character, int Amount);
	UFUNCTION(BlueprintCallable,Category=Update)
	void LoadIni();
	UFUNCTION(BlueprintCallable,Category=Update)
	void SaveIni();
	
	
	UFUNCTION(BlueprintCallable,BlueprintPure,Category=Option)
	float GetMasterVolume() const;
	UFUNCTION(BlueprintCallable,BlueprintPure,Category=Option)
	float GetSFXVolume() const;
	UFUNCTION(BlueprintCallable,BlueprintPure,Category=Option)
	float GetVoiceVolume() const;
	UFUNCTION(BlueprintCallable,BlueprintPure,Category=Option)
	float GetBGMVolume() const;
	UFUNCTION(BlueprintCallable,Category=Option)
	void SetMasterVolume(float Value);
	UFUNCTION(BlueprintCallable,Category=Option)
	void SetSFXVolume(float Value);
	UFUNCTION(BlueprintCallable,Category=Option)
	void SetVoiceVolume(float Value);
	UFUNCTION(BlueprintCallable,Category=Option)
	void SetBGMVolume(float Value);

	UFUNCTION(BlueprintCallable,Category=GameState)
	void SetGameState(int State);
	UFUNCTION(BlueprintCallable,BlueprintPure,Category=GameState)
	int GetGameState()const;	
	

	UPROPERTY(BlueprintAssignable,Category=Update)
	FOnUpdateCount OnUpdateCount;
};
