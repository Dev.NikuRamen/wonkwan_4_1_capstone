// Copyright Epic Games, Inc. All Rights Reserved.

#include "Capstone_4_TopdownGameMode.h"
#include "Capstone_4_TopdownPawn.h"

ACapstone_4_TopdownGameMode::ACapstone_4_TopdownGameMode()
{
	// set default pawn class to our character class
	DefaultPawnClass = ACapstone_4_TopdownPawn::StaticClass();
}

