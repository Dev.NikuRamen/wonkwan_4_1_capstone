// Copyright Epic Games, Inc. All Rights Reserved.

#include "Capstone_4_Topdown.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Capstone_4_Topdown, "Capstone_4_Topdown" );

DEFINE_LOG_CATEGORY(LogCapstone_4_Topdown)
 