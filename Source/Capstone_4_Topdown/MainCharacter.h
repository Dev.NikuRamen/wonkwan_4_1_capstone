// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHitEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDieEvent);

UCLASS()
class CAPSTONE_4_TOPDOWN_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category=Health)
	float MaxHealth;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Health)
	float Health;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Health)
	bool bIsDead;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=GameState)
	bool bIsClear;

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category=Health)
	void OnHit(int Damage,FVector Direction);
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category=Health)
	void OnDie();
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable,Category=Health)	
	void OnClear();
	

	UPROPERTY(BlueprintAssignable,Category=Health)
	FOnHitEvent OnHitEvent;
	UPROPERTY(BlueprintAssignable,Category=Health)
	FOnDieEvent OnDieEvent;
	
};
