// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Capstone_4_TopdownGameMode.generated.h"

UCLASS(MinimalAPI)
class ACapstone_4_TopdownGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACapstone_4_TopdownGameMode();
};



