// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHUD_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class CAPSTONE_4_TOPDOWN_API UPlayerHUD_UserWidget : public UUserWidget
{
	GENERATED_BODY()


public:
	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable,Category=UI)
	void OnUpdateUI();
};
