// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class CAPSTONE_4_TOPDOWN_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Health)
	int Health;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category=Health)
	int MaxHealth;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category=Health)
	bool IsDead;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Combat)
	bool IsCombat;
	
	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	UFUNCTION(BlueprintCallable,BlueprintNativeEvent,Category=Health)
	void OnHit(const int Damage);
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent,Category=Health)
	void OnHeal(const int Amount);
	UFUNCTION(BlueprintNativeEvent,Category=Health)
	void OnDie();
	UFUNCTION(BlueprintCallable,BlueprintNativeEvent,Category=Attack)
	void OnAttack();
};
