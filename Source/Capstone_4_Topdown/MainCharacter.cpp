// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MaxHealth = 100;
	Health = 100;

}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMainCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	OnHitEvent.Clear();
	OnDieEvent.Clear();
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMainCharacter::OnHit_Implementation(const int Damage,const FVector Direction)
{
	Health -= Damage;
	if(Health <= 0)
	{
		OnDie();
	}

	if(OnHitEvent.IsBound() == true)
	{
		OnHitEvent.Broadcast();
	}
}

void AMainCharacter::OnClear_Implementation()
{
	bIsClear = true;
}

void AMainCharacter::OnDie_Implementation()
{
	bIsDead = true;
	Health = 0;

	if(OnDieEvent.IsBound() == true)
	{
		OnDieEvent.Broadcast();
	}
}
