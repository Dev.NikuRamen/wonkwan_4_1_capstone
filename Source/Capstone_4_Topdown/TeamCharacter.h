// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainCharacter.h"
#include "TeamCharacter.generated.h"

/**
 * 
 */
UCLASS()
class CAPSTONE_4_TOPDOWN_API ATeamCharacter : public AMainCharacter
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Team)
	bool IsRescued;

public:
	UFUNCTION(BlueprintCallable,BlueprintGetter,Category=Team)
	bool GetIsRescued();
};
